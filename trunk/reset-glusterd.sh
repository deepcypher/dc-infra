#!/usr/bin/env bash

# @Author: GeorgeRaven <archer>
# @Date:   2021-05-06T16:28:28+01:00
# @Last modified by:   archer
# @Last modified time: 2021-05-06T16:29:36+01:00
# @License: please see LICENSE file in project root

sudo systemctl stop glusterd
sudo cp /var/lib/glusterd/glusterd.info ~/. || exit 1
sudo rm -r /var/lib/glusterd/*
sudo cp ~/glusterd.info /var/lib/glusterd/.
sudo systemctl start glusterd
