#!/usr/bin/env bash

# @Author: GeorgeRaven <archer>
# @Date:   2021-05-02T12:51:19+01:00
# @Last modified by:   archer
# @Last modified time: 2021-05-04T19:51:52+01:00
# @License: please see LICENSE file in project root

# delete the repository source which held the kustomization
flux delete source git dc

# delete our secrets
helm uninstall dc-secret
