# dc-infra

Deep Cypher flux kubernetes cluster. Describing all of our resources under one unifying repository with various overlays depending on the deployment scenario.
This will set up a clusters infrastructure (with the notable exception of volumes), all associated applications to deploy inside the cluster, and our EDLaaS API.

This is heavily in production to bring our existing work together so please bear with us. We want to do it right which will take us quite some time to get there. Privacy matters.

WARNING: This cluster of resources is heavily in development, there may be many continually breaking changes. While we are happy to manually correct these issues please for the love of all that is privacy, don't use this repo as an ACTIVE/ continually synced resource as by god things are going to break for a long time yet (And all credentials are completely randomized whenever a resource is re-created, or secrets re-generated for whatever reason like lets say we added a WHOLE LOAD MORE). You are probably fine to SUSPEND this resource in a static state, but be-aware you will likely need to reset literally everything when its finally time to settle in for the long haul of flipping this worlds privacy nightmares into at-least manageable chaos!

If you did want to use this cluster:
- install kubectl
- install flux v2
- install helm
- have a cluster/ create a cluster (kubeadm, minikube, k3s, whatever tool you like)
- run all \*-init-\*.sh scripts in order

If you want to tear everything down:
- run all \*-rm-\*.sh scripts
