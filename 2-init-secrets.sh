#!/usr/bin/env bash

# @Author: GeorgeRaven <archer>
# @Date:   2021-05-02T12:51:19+01:00
# @Last modified by:   archer
# @Last modified time: 2021-05-04T19:47:40+01:00
# @License: please see LICENSE file in project root

# generate our secrets
helm install dc-secret secrets/.
