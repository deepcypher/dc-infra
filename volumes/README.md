# Volumes

This dir/ helm chart will hold customizable volumes designed to be run once, and forgotten. You almost certainly dont want to mess with these after they have been created/ are in use unless you know what you are doing. As a bare minimum just make sure these volumes are on nodes that actually exist for you, and are pointing to paths that you would actually be comfortable with being used for kube purposes.

The intention is to manage a second set of volumes inside the kubernetes cluster by creating local volumes, then broadcasting them using glusterFS as persistent volume classes that we can then play on top of as much as we like, but the base pods will never change (in theory).
