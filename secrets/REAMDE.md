# Secrets .. shhh

This dir/ helm chart enables easy customization of cluster secrets using sting randomization.
The intention is that this is run once initially on cluster set-up, but it would also in theory be possible to call it later, although beware that you will want to disable the default values else those secrets will be overwritten.

Simply clone/ download, modify values.yaml to add whatever secrets you desire and execute:

```
helm install dc-secrets ./secrets/
```

We also provide a shell .sh script to install and install this helm chart of secrets.
